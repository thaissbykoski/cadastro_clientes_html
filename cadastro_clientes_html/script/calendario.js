$(function(){
  $("#calendario").datepicker({
    showOn: "button",
    buttonImage: "imagens/calendar.png",
    buttonImageOnly: true,
    showButtonPanel: true,
    dateFormat: 'dd-mm-yy',
    changeMonth: true,
    changeYear: true,
     yearRange: "-100y:+0y",
    showOthersMonths: true,
    selectOtherMonths: true,
    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado', 'Domingo'],
    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab', 'Dom'],
    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
  });
});
